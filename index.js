
// This code will hep us access contents of express module/package
// A "module" is a software component or part of a proglem that contians one or more routines
// It also allows us to access methods and functions that we will use to easily create an app or server

// We store our express module to variable so we could easily access its keywords, functions, and methods.

const express = require('express');

// This code creates an application using express / aka express application
	// App is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applying the option of "extended: true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));

// This route expects to receive a GET request at the URI/endpoint
app.get("/hello", (req, res) => {

	// This is the response that we will expect to receive if the get method is successful with the right endpoint
	res.send('GET method success. \n Hello from the /hello endpoint!');

});


app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
});

let users = [];


app.post("/signup", (req, res) => {
	if (req.body.username !== "" && req.body.password !== "") {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered.`);
		console.log(users);

		// only the first resposne will display. 
		// res.send("Welcome admin");

	} else {
		res.send("Please input BOTH username and password");
	}
});


app.put('/change-password', (req, res) => {
	let message;

	if (users.length == 0) {
		res.send("Currently no users registered");
	}

	for(let i = 0; i < users.length; i++) {
		if(req.body.username == users[i].username) {
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated`;
			break;
		} else {
			message = `User does not exit.`;
		}
	}

	console.log(users);

	res.send(message);
});

// 1
// Create a route that expect GET request at the uri/endpoint "/home"
// Response that will be received should be "Welcome to the homepage"
// Code below...

app.get("/home", (req, res) => {
	res.send("Welcome to the homepage");
})


// 2
// Create a reoute that expects GET request at the uri/endpoint "/users"
// Response that will be received is a collection of users

app.get("/users", (req, res) => {
	if (users.length == 0) {
		res.send("No users registered.");
	} else {
		res.send(users);
	}

	
});



// 3 
// Create a route that expects DELETE request at the uri/endpoint "/delete-user"
// The program should check if the user is existing before deleting
// Response that will be received is the updated collection of users

app.delete("/delete-user", (req, res) => {

	const username = req.body.username;
	const password = req.body.password;

	if (users.length == 0) {
		res.status(404).send(`User with username ${username} does not exist or password is incorrect. Deletion unsuccessful.`);


	} else {



	for(let i=0; i < users.length; i++) {

		if (username == users[i].username && password == users[i].password) {

			users.splice(i, 1);
			res.status(200).send({
				status: "ok",
				users: users
			})
			return;

		} 


		}
	}

	res.status(404).send(`User with username ${username} does not exist or password is incorrect. Deletion unsuccessful.`);


	

});

// 4
// Save all the successful request tabs and export collections
// Upload/update yoru gitlab repo and link it to Boodle




// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal

app.listen(port, () => console.log(`Server running at port ${port}`));